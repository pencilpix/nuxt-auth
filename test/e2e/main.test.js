const { Nuxt, Builder } = require('nuxt')
const request = require('request-promise-native')

const config = require('../fixture/nuxt.config')

const PORT = 4000
const TIMEOUT = 50000
const url = path => `http://localhost:${PORT}${path}`
const get = path => request(url(path))

let nuxt

module.exports = {
  // stub
  before: async function (browser, done) {
    nuxt = new Nuxt(config)
    await new Builder(nuxt).build()
    await nuxt.listen(PORT)

    done()
  },


  'check auth state at home page and user is not logged': function (client) {
    client
      .url(url('/'))
      .waitForElementVisible('body')
      .waitForElementVisible('div#auth')
      .getText('div.isAuth', function ({ value }) {
        this.assert.equal(value, 'false')
      })
      .getText('div.isClient', function ({ value }) {
        this.assert.equal(value, 'true')
      })
      .getText('div.user', function ({ value }) {
        this.assert.equal(value, '')
      })
      .deleteCookies()
      .end()
  },


  'check cookies for client authentication': function (client) {
    client
      .url(url('/'))
      .waitForElementVisible('body', 1000)
      .getCookie('$auth.isAuth', function ({ value }) {
        this.assert.equal(value, 'false')
      })
      .getCookie('$auth.isClient', function ({ value }) {
        this.assert.equal(value, 'true')
      })
      .getCookie('$auth.token', function ({ value }) {
        this.assert.equal(value, 'someclienttoken')
      })
      .deleteCookies()
      .end()
  },


  'should redirect to login': function(client) {
    client
      .url(url('/secret'))
      .waitForElementVisible('body', 1000)
      .waitForElementVisible('#login', 1000)
      .url(function ({ value }) {
        this.assert.equal(value, url('/login') + '?redirect=%2Fsecret')
      })
      .deleteCookies()
      .end()
  },


  'should login and redirect to home': function (client) {
    client
      .url(url('/login'))
      .waitForElementVisible('#login')
      .setValue('#username', 'pencilpix@gmail.com')
      .setValue('#password', 'asldfjsdklf')
      .click('#loginButton')
      .waitForElementVisible('#homeUser')
      .getText('#homeUser', function ({ value }) {
        this.assert.equal(value, 'pencilpix')
      })
      .deleteCookies()
      .end()
  },

  'should login and redirect to secret': function (client) {
    client
      .url(url('/secret'))
      .waitForElementVisible('#login')
      .setValue('#username', 'pencilpix@gmail.com')
      .setValue('#password', 'asldfjsdklf')
      .click('#loginButton')
      .waitForElementVisible('#secret')
      .getText('#secret', function ({ value }) {
        this.assert.equal(value, 'secret')
      })
      .deleteCookies()
      .end()
  },

  'should redirect to login after logout if route is authenticated': function (client) {
    client
      .url(url('/secret'))
      .waitForElementVisible('#login')
      .setValue('#username', 'pencilpix@gmail.com')
      .setValue('#password', 'asldfjsdklf')
      .click('#loginButton')
      .waitForElementVisible('#logout')
      .click('#logout')
      .waitForElementVisible('#login')
      .deleteCookies()
      .end()
  },

  'should show error and not redirect': function (client) {
    client
      .url(url('/secret'))
      .waitForElementVisible('#login')
      .setValue('#username', 'dummymail@gmail.com')
      .setValue('#password', 'asldfjsdklf')
      .click('#loginButton')
      .waitForElementVisible('#error')
      .getText('#error', function ({ value }) {
        this.assert.equal(value, 'error')
      })
      .deleteCookies()
      .end()
  },




  // teardown
  after: async function() {
    await nuxt.close()
  },
}

