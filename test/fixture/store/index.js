import Vuex from 'vuex'
import Vue from 'vue'

const createStore = () => {
  return new Vuex.Store({
    state: {
      users: {},
      currentUser: null,
    },
    mutations: {
      LOAD_USER(state, payload) {
        Vue.set(state.users, payload.id, payload)
      },

      SET_CURRENT(state, payload) {
        Vue.set(state, 'currentUser', payload)
      },
    },
    getters: {
      user: ({ users, currentUser }) => users[currentUser],
    },
    actions: {
      loadUser({ commit }, payload) {
        commit('LOAD_USER', payload)
        return Promise.resolve()
      },

      setCurrent({ commit }, payload) {
        commit('SET_CURRENT', payload)
        return Promise.resolve()
      }
    },
  })
}

export default createStore

