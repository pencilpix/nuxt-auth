export default function logout (ctx) {
  const $auth = ctx.app.$auth || { on: () => null }

  $auth.on('success', (data, topic) => {
    if (topic === 'logout') {
      ctx.store.dispatch('setCurrent', null)
    }
  })
}

