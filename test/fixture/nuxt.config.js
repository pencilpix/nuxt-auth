const { resolve } = require('path')

module.exports = {
  rootDir: resolve(__dirname, '../..'),
  srcDir: __dirname,
  dev: false,
  render: {
    resourceHints: false
  },
  modules: ['@@', '@nuxtjs/axios'],

  plugins: ['~/plugins/logout.js'],

  // auth config
  auth: {
    client: {
      client_id: 1,
      client_secret: 'clientsecret',
      grant_type: 'client_credentials',
      scope: '*'
    },

    personal: {
      client_id: 2,
      client_secret: 'passsecret',
      grant_type: 'password',
      scope: '*'
    },

    endpoints: {
      host: 'http://localhost:53624',
      login: '/oauth/token',
      authorize: '/oauth/token',
      logout: '/api/logout',
      user: '/api/users/user'
    },

    onFetch: (topic, store, data) => {
      if (topic === 'user/success' && data.content) {
        store.dispatch('loadUser', data.content)
        store.dispatch('setCurrent', data.content.id)
      }
    },
  },

  axios: {
    proxy: false
  },

  proxy: {
    '/api': 'http://localhost:53624'
  }
}
