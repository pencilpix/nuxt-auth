import Vue from 'vue'
import Cookies from 'js-cookie'
import cookie from 'cookie'

export default class Auth {
  constructor(ctx, options, initStore) {
    this.ctx = ctx,
    this.options = options
    this.initStore = initStore
    this.redirect = '/'
    this.options.onFetch = this.options.onFetch || (() => {})
    this.$errorListeners = []
    this.$successListeners = []
    Vue.set(this, '$state', {})
  }


  //////////////////////////////////////////////////////
  // public methods
  //////////////////////////////////////////////////////
  init() {
    if (!this.ctx.store) {
      return Promise.reject('[$auth] needs vuex store to work correctly')
    }

    if (!this.$isClientAndPersonal()) {
      return Promise.reject('[$auth] the config of client and personal token must be set in module options')
    }

    this.initStore(this.ctx)

    // check if already there is a token
    // set cookies and header and store.
    this.$syncToken()

    this.ctx.app.$axios.onRequest((config) => {
      const access = this.$globalGet('type')
      const token = this.$globalGet('token')

      if (this.$isSet(access) && this.$isSet(token)) {
        config.headers.common['Authorization'] = access + ' ' + token
      } else {
        delete config.headers.common['Authorization']
      }
    })


    if (!process.client) return Promise.resolve(true)

    window.addEventListener('storage', () => {
      const { isAuth } = JSON.parse(window.localStorage.getItem('$auth'))
      const stateIsAuth = this.getState('isAuth')

      if (isAuth && isAuth !== stateIsAuth) {
        this.$syncToken()
        this.fetchUser()
      } if (!isAuth && isAuth !== stateIsAuth) {
        this.$setKeys(null, null, '')
        this.setState('user', null)
        this.options.onFetch('logout/success', this.ctx.store)
        this.authorize()
      }
    })

    return Promise.resolve(true)
  }


  authorize() {
    const isToken = this.$isSet(this.$token)
    const isAuthorized = this.$isAuth || this.$isClient

    if (isToken && isAuthorized) {
      return Promise.resolve()
    }

    return this.$fetch({
      url: '/oauth/authorize',
      method: 'post',
      data: {
        ...this.options.client
      }
    }, 'authorize').then(res => {
      const { access_token, token_type, expires_in } = res
      this.$setKeys(access_token, token_type, 'client', expires_in)
      this.options.onFetch('authorize/success', this.ctx.store, res)
    })
  }


  login(credentials) {
    if (this.$isLoading || (this.$isAuth && this.$isSet(this.$token))) {
      return Promise.resolve()
    }

    return this.$fetch({
      url: '/oauth/login',
      method: 'post',
      data: {
        ...this.options.personal,
        ...credentials
      }
    }, 'login').then(res => {
      const { access_token, token_type, expires_in } = res
      this.$setKeys(access_token, token_type, 'auth', expires_in)
      this.options.onFetch('login/success', this.ctx.store, res)
      if (this.options.loadUserAfterLogin) return this.fetchUser()
      return res
    }).then(() => {
      const { route } = this.ctx;
      const params = route.params || {}
      const lang = params.lang || params.locale || ''

      if (this.options.autoRedirect) {
        this.ctx.redirect(route.query.redirect || (this.redirect + lang))
      }
    })
  }

  logout() {
    return this.$fetch({
      url: '/oauth/logout',
      method: 'post'
    }, 'logout').then(res => {
      const authRoutes = this.getState('authenticatedRoutes')
      this.$setKeys(null, null, '')
      this.setState('user', null)
      this.options.onFetch('logout/success', this.ctx.store, res)
      if (authRoutes[this.ctx.route.name] && this.options.autoRedirect) {
        this.ctx.redirect(this.loginRoute())
      }
    })
  }


  fetchUser() {
    if (this.$user) return Promise.resolve()

    return this.$fetch({ url: '/oauth/user', method: 'get'}, 'user').then(res => {
      const { content, id } = res
      const isUser = this.$isSet(id) || this.$isSet(content && content.id)

      if (!isUser) {
        this.$setKeys(null, null, '')
        this.setState('user', null)
        return this.authorize()
      }

      this.setState('user', (content && content.id) || id)
      this.options.onFetch('user/success', this.ctx.store, res)
      return res
    }).catch(err => {
      if (err) {
        if (process.client) console.log(err)
        this.$setKeys(null, null, '')
        this.setState('user', null)
        return this.authorize()
      }
      return null
    })
  }


  loginRoute() {
    const { loginRoute } = this.options
    const { lang, locale } = this.ctx.route.params
    let redirectRoute = {
      query: { redirect: this.ctx.route.fullPath },
      params: {}
    }

    if (loginRoute && typeof loginRoute === 'object') {
      redirectRoute = Object.assign({}, loginRoute, redirectRoute)
    } else {
      redirectRoute.path = loginRoute || '/login'
    }

    if (lang) redirectRoute.params.lang = lang
    else if (locale) redirectRoute.params.locale = locale

    return redirectRoute
  }


  getState(getter) {
    return this.ctx.store.getters['$auth/' + getter]
  }


  setState(key, value) {
    return this.ctx.store.dispatch('$auth/set', { key, value })
  }

  setStorage(key, value, expires) {
    const isClient = process.client
    const auth = (isClient && JSON.parse(localStorage.getItem('$auth'))) || {}

    if (!isClient) return
    auth[key] = value
    if (typeof expires !== 'undefined') auth.expiration = expires
    localStorage.setItem('$auth', JSON.stringify(auth))
  }

  getStorage(key) {
    const isClient = process.client
    const auth = (isClient && JSON.parse(localStorage.getItem('$auth'))) || {}
    if (!isClient) return null
    return auth[key]
  }


  setCookie(key, value, expires) {
    if (process.server) return

    if (!this.$isSet(value)) {
      Cookies.remove('$auth.' + key)
    } else {
      Cookies.set('$auth.' + key, value, { expires: Number(expires) })
    }
  }


  getCookie(key, isJson) {
    const $key = '$auth.' + key
    const { req } = this.ctx
    const cookiesStr = process.client ? document.cookie : req ? req.headers.cookie : ''
    const $cookies = cookie.parse(cookiesStr || '') || {}
    const value = $cookies[$key]
    return isJson ? JSON.parse(value) : value
  }


  on(type, cb) {
    if (!type || (type !== 'success' && type !== 'error')) {
      console.warn('[$auth]: there is no event called ' + type)
      return
    }

    if (typeof cb !== 'function') {
      console.error('[$auth]: callback listener must be a function')
      return
    }

    this['$' + type + 'Listeners'].push(cb)
  }



  //////////////////////////////////////////////////////
  // private methods
  //////////////////////////////////////////////////////
  get $isLoading() {
    return this.getState('loading')
  }


  get $token() {
    return this.$globalGet('token')
  }


  get $isAuth() {
    return this.$globalGet('isAuth')
  }


  get $isClient() {
    return this.$globalGet('isClient')
  }


  get $user() {
    return this.getState('user')
  }


  $isClientAndPersonal() {
    let isConfigValid = true
    const {personal, client} = this.options || {}

    if (!personal || !client) {
      return false
    }

    [personal, client].forEach(options => {
      Object.keys(options).forEach(option => {
        if (!this.$isSet(options[option])) {
          isConfigValid = false
        }
      })
    })
    return isConfigValid
  }


  $isSet(value) {
    return typeof value !== 'undefined' && value !== null
  }



  $fetch(request, topic) {
    if (!request.url) {
      return Promise.reject('[$auth]: there is no request to fetch')
    }

    if (!request.method) {
      return Promise.reject('[$auth]: there is no method type in the request, ' + request)
    }

    if (this.$isLoading) return Promise.reject('[$auth] loading')

    if (process.client) this.setState('loading', true)


    return this.ctx.app.$axios.request(request).then(res => {
      if (process.client) this.setState('loading', false)
      this.$callListeners('success', res.data, topic)
      return res.data
    }).catch(err => {
      if (process.client) this.setState('loading', false)
      this.$callListeners('error', err, topic)
      return Promise.reject(err)
    })
  }


  $callListeners(type, arg, topic) {
    this['$' + type + 'Listeners'].forEach(listener => {
      listener(arg, topic)
    })
  }


  $syncToken() {
    const token = this.$globalGet('token')
    const isClient = this.$globalGet('isClient')
    const isAuth = this.$globalGet('isAuth')
    const expires = this.$globalGet('expiration') || ''

    if (this.$isSet(token)) {
      this.$globalSet('token', token, expires)
      this.$globalSet('isClient', isClient, expires)
      this.$globalSet('isAuth', isAuth, expires)
      if (expires) {
        this.$globalSet('expiration', expires, expires)
      }
    }
  }


  $setKeys(token, access, type, expiration) {
    let expires = this.$globalGet('expiration')

    if (expiration) {
      expires = new Date(expiration * 1000).getTime() / (1000 * 60 * 60 * 24);
    }

    this.$globalSet('token', token, expires)
    this.$globalSet('type', access, expires)
    this.$globalSet('isAuth', type === 'auth', expires)
    this.$globalSet('isClient', type === 'client', expires)
    this.$globalSet('expiration', expires, expires)
  }


  $globalGet(key) {
    let value = this.getStorage(key)
    if (this.$isSet(value)) return this.$getBoolStr(value)
    value = this.getCookie(key)
    if (this.$isSet(value)) return this.$getBoolStr(value)
    value = this.getState(key)
    return this.$getBoolStr(value)
  }


  $globalSet(key, value, expires) {
    const val = this.$getBoolStr(value);
    const exp = parseInt(expires, 10)
    this.setCookie(key, val, exp || null)
    this.setState(key, val)
    this.setStorage(key, val, exp || null)
    Vue.set(this.$state, key, val)
  }

  $getBoolStr(str) {
    const isBool = typeof str === 'string' && /(true|false)/i.test(str)

    if (isBool && str === 'false') return false;
    if (isBool &&  str === 'string' && str === 'true') return true;
    return str;
  }
}
