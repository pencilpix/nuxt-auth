import Vue from 'vue'

const types = {
  SET: 'SET',
  RESET: 'RESET',
}


const mutations = {
  [types.SET](state, payload) {
    const { key, value } = payload
    Vue.set(state, key, value)
  },

  [types.RESET](state) {
    Object.keys(state).forEach(key => {
      if (typeof state[key] === 'boolean') {
        Vue.set(state, key, false)
      } else {
        Vue.set(state, key, '')
      }
    })
  }
}



const getters = {
  isAuth: state => state.isAuth,
  isClient: state => state.isClient,
  loading: state => state.loading,
  user: state => state.user,
  token: state => state.token,
  authType: state => state.isAuth ? 'user' : state.isClient ? 'client' : '',
  expiration: state => state.expiration,
  authenticatedRoutes: state => state.authenticatedRoutes,
}


const actions = {
  set({ commit }, payload) {
    commit(types.SET, payload)
    return Promise.resolve()
  },

  reset({ commit }) {
    commit(types.RESET)
    return Promise.resolve()
  },
}

const createAuthStore = () => ({
  namespaced: true,
  state: {
    isAuth: null,
    isClient: null,
    loading: null,
    token: null,
    user: null,
    expiration: null,
    authenticatedRoutes: {},
  },

  actions,
  mutations,
  getters,
})


export default function initAuthStore ({ app, store }) {
  const opts = {}

  if (process.client) {
    opts.preserveState = true
  }

  store.registerModule('$auth', createAuthStore(), opts)
}
