import Auth from './auth.class'
import initAuthStore from './auth.store'
import './authenticate.middleware'

export default async function (ctx, inject) {
  /* eslint-disable */
  const onFetch = <%= options.onFetch %>
  const options = <%= JSON.stringify(options) %>
  let $auth = null
  let isAuth = null
  let user = null

  // pass onFetch function with options.
  // and create the $auth instance.
  options.onFetch = onFetch
  $auth = new Auth(ctx, options, initAuthStore)


  // make $auth available
  // in app and global in Vue
  inject('auth', $auth)


  // get user or authorize the client
  isAuth = await $auth.init().then(() => $auth.$isAuth)
    .catch(err => process.client ? err : null)

  if (isAuth) {
    user = await $auth.fetchUser()
      .catch(err => process.client ? err : null)
  } else {
    user = await $auth.authorize()
      .catch(err => process.client ? err : null)
  }
}
