import Middleware from './middleware'

Middleware['authenticate'] = function ({ app, store, redirect, route }) {
  const { $auth } = app
  const authRoutes = Object.assign({}, $auth.getState('authenticatedRoutes'))
  authRoutes[route.name] = true

  $auth.setState('authenticatedRoutes', authRoutes)

  if (!$auth.$isAuth) {
    return redirect($auth.loginRoute())
  }
}

