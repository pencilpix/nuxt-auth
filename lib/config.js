module.exports = {
  DEFAULTS:{
    loadUserAfterLogin: true,
    autoRedirect: true,
    loginRoute: { name: 'login' },
    client: {
      client_id: null,
      client_secret: null,
      grant_type: null,
      scope: null
    },

    personal: {
      client_id: null,
      client_secret: null,
      grant_type: null,
      scope: null
    },

    endpoints: {
      host: 'http://localhost:3000',
      login: '/oauth/token',
      authorize: '/oauth/token',
      logout: '/auth/api/logout',
      user: '/api/users/'
    },
  },
}
