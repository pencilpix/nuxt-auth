const { resolve } = require('path')
const { DEFAULTS } = require(resolve(__dirname, './config.js'))


module.exports = async function module (moduleOptions) {
  const options = Object.assign({}, DEFAULTS, moduleOptions, this.options.auth)

  this.options.axios = this.options.axios || {}
  this.options.axios.proxy = true
  this.options.proxy = this.options.proxy || {}
  this.options.proxy['/oauth'] = {
    target: options.endpoints.host,
    pathRewrite: {
      '^/oauth/login': options.endpoints.login,
      '^/oauth/authorize': options.endpoints.authorize,
      '^/oauth/logout': options.endpoints.logout,
      '^/oauth/user': options.endpoints.user,
    }
  }


  this.nuxt.hook('build:before', (ctx) => {
    ctx.nuxt.moduleContainer.addTemplate({
      src: resolve(__dirname, './_store/index.js'),
      fileName: 'auth.store.js',
      options
    })
  })


  this.addTemplate({
    src: resolve(__dirname, './auth.class.js'),
    fileName: 'auth.class.js',
    options
  })


  this.addPlugin({
    src: resolve(__dirname, './_plugins/index.js'),
    fileName: 'auth.plugin.js',
    options
  })

  this.addTemplate({
    src: resolve(__dirname, './_middlewares/authenticate.js'),
    fileName: 'authenticate.middleware.js',
    options
  })


  this.requireModule(['@nuxtjs/axios'])
}

module.exports.meta = require('./package.json')
